#pragma once

#include <stack>

#include "font.h"
#include "sprite.h"

class Menu;

struct MenuEntry{
	const char* name;
	void (*action)(Menu* menu, void* data);
	void* data;
};

class Menu{
	MenuEntry*   entries;
	unsigned     num_entries;
	unsigned     width; // in characters
	Font*        font;
	SpriteAtlas* deco; // decorations
	
	SDL_Surface* dest;

	unsigned     current_entry;

	int          last_cursor_x, last_cursor_y;
public:
	const char*  title;
	int          initial_x, initial_y;

	void open(SDL_Surface* p_dest, int x, int y);
	void draw();
	void cursorUp();
	void cursorDown();
	void choose();
	void erase();
	SDL_Surface* getDest() const;

	Menu(const char* p_title,
		 MenuEntry* p_entries,
		 unsigned p_num_entries,
		 Font* p_font,
		 SpriteAtlas* p_deco);
};

void menuStubAction(Menu* menu, void* data);
void menuOpenAction(Menu* menu, void* data);
void menuCloseAction(Menu* menu,void* data);
void menuLaunchAction(Menu* menu,void* data);

void menuClose();
void menuCursorUp();
void menuCursorDown();
void menuChoose();
bool menuMultipleOpen();
