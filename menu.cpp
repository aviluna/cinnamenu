#include <iostream>
#include <cstdlib>
#include <cstring>

#include "menu.h"

using namespace std;

stack<Menu*> MenuStack;

// deco atlas is 8 chars wide
// therefore use octal to identify tiles
enum DecoId{
	DECO_SPACE          = 000,
	TOP_LEFT_BORDER     = 001,
	TOP_RIGHT_BORDER    = 002,
	BOTTOM_LEFT_BORDER  = 011,
	BOTTOM_RIGHT_BORDER = 012,
	HORIZONTAL_BORDER   = 003,
	VERTICAL_BORDER     = 004,
	TITLE_OPEN          = 013,
	TITLE_CLOSE         = 014,
	CURSOR_NORMAL       = 005,
	CURSOR_SELECTED     = 015
};

const unsigned DECO_WIDTH  = 8;
const unsigned DECO_HEIGHT = 12;

void Menu::open(SDL_Surface* p_dest, int x, int y){
	dest = p_dest;
	initial_x = x;
	initial_y = y;
	this->erase();
	this->draw();
	MenuStack.push(this);
}

void Menu::draw(){
	int curx = 0, cury = 0;

#define MDRAW(__which) deco->drawSprite(\
		dest, \
		initial_x + (curx * DECO_WIDTH), \
		initial_y + (cury * DECO_HEIGHT), \
		__which); \
	curx++

#define MLINE() curx = 0; cury++

	MDRAW(TOP_LEFT_BORDER);
	MDRAW(TITLE_OPEN);

	font->drawText(dest,
			       initial_x + (curx * DECO_WIDTH),
				   initial_y + (cury * DECO_HEIGHT),
				   title);

	curx += strlen(title);

	MDRAW(TITLE_CLOSE);

	while(curx < (width - 1)){
		MDRAW(HORIZONTAL_BORDER);
	}

	MDRAW(TOP_RIGHT_BORDER);

	MLINE();

	for(unsigned i = 0; i < num_entries; i++){
		MDRAW(VERTICAL_BORDER);
		
		if(i == current_entry){
			last_cursor_x =
				initial_x + (curx * DECO_WIDTH);
			last_cursor_y =
				initial_y + (cury * DECO_HEIGHT);
			MDRAW(CURSOR_NORMAL);
		}
		else{
			MDRAW(DECO_SPACE);
		}

		font->drawText(dest,
				       initial_x + (curx * DECO_WIDTH),
					   initial_y + (cury * DECO_HEIGHT),
					   entries[i].name);
		curx = width-1;

		MDRAW(VERTICAL_BORDER);
		
		MLINE();
	}

	MDRAW(BOTTOM_LEFT_BORDER);

	while(curx < (width - 1)){
		MDRAW(HORIZONTAL_BORDER);
	}

	MDRAW(BOTTOM_RIGHT_BORDER);

#undef MDRAW
#undef MLINE
}

void Menu::cursorUp(){
	if(current_entry == 0){
		// don't go higher than the top of the list
		return;
	}
	deco->drawSprite(dest,
			         last_cursor_x,
					 last_cursor_y,
					 DECO_SPACE);

	last_cursor_y -= DECO_HEIGHT;
	current_entry--;

	deco->drawSprite(dest,
			         last_cursor_x,
					 last_cursor_y,
					 CURSOR_NORMAL);
}

void Menu::cursorDown(){
	if((current_entry + 1) == num_entries){
		// don't go lower than the end of the list
		return;
	}
	deco->drawSprite(dest,
			         last_cursor_x,
					 last_cursor_y,
					 DECO_SPACE);

	last_cursor_y += DECO_HEIGHT;
	current_entry++;

	deco->drawSprite(dest,
			         last_cursor_x,
					 last_cursor_y,
					 CURSOR_NORMAL);
}

void Menu::choose(){
	deco->drawSprite(dest,
			         last_cursor_x,
					 last_cursor_y,
					 CURSOR_SELECTED);

	MenuEntry* entry = entries + current_entry;

	entry->action(this, entry->data);
}

SDL_Surface* Menu::getDest() const{
	return dest;
}

void Menu::erase(){
	SDL_Rect dst = {
		.x = initial_x,
		.y = initial_y,
		.w = width * DECO_WIDTH,
		.h = (num_entries+2) * DECO_HEIGHT
	};
	SDL_FillRect(dest, &dst, 0);
}

Menu::Menu(const char* p_title,
		   MenuEntry* p_entries,
		   unsigned p_num_entries,
		   Font* p_font,
		   SpriteAtlas* p_deco){
	title = p_title;
	entries = p_entries;
	num_entries = p_num_entries;
	font = p_font;
	deco = p_deco;

	unsigned longest = strlen(title);
	for(unsigned i = 0; i < num_entries; i++){
		unsigned length = strlen(entries[i].name);
		if(length > longest)
			longest = length;
	}

	// add 4 to leave room for border and title decorators
	// cursor will thus also have room
	width = longest + 4;

	current_entry = 0;
}

void menuClose(){
	if(!MenuStack.empty()){
		MenuStack.top()->erase();
		MenuStack.pop();
		if(!MenuStack.empty()){
			MenuStack.top()->draw();
		}
		else{
			exit(0); // closed root menu
		}
	}
}
void menuCloseAction(Menu* menu, void* data){
	menuClose();
}

void menuOpenAction(Menu* menu, void* data){
	Menu* new_menu = reinterpret_cast<Menu*>(data);
	new_menu->open(menu->getDest(),
			       menu->initial_x + 48,
				   menu->initial_y + 36);
}

void menuStubAction(Menu* menu, void* data){
	cerr << "menu stub from " << menu->title << "!" << endl;
}

void menuLaunchAction(Menu* menu, void* data){
	const char* system_string = reinterpret_cast<const char*>(data);
	system(system_string);
}

void menuCursorUp(){
	if(!MenuStack.empty()){
		MenuStack.top()->cursorUp();
	}
}

void menuCursorDown(){
	if(!MenuStack.empty()){
		MenuStack.top()->cursorDown();
	}
}

void menuChoose(){
	if(!MenuStack.empty()){
		MenuStack.top()->choose();
	}
}

bool menuMultipleOpen(){
	return (MenuStack.size() > 1);
}
