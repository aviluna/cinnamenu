CXX := g++
CXXFLAGS := -Os -g
INCLUDES := -I/usr/include/SDL/
LIBS := -lSDL

all: main

font.o: font.cpp
	$(CXX) -c -o $@ $(INCLUDES) $(CXXFLAGS) $^

menu.o: menu.cpp
	$(CXX) -c -o $@ $(INCLUDES) $(CXXFLAGS) $^

sprite.o: sprite.cpp
	$(CXX) -c -o $@ $(INCLUDES) $(CXXFLAGS) $^

main.o: main.cpp
	$(CXX) -c -o $@ $(INCLUDES) $(CXXFLAGS) $^

main: main.o sprite.o menu.o font.o
	$(CXX) -o $@ $(LIBS) $(INCLUDES) $^ $(CXXFLAGS)

