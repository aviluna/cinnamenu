#pragma once

#include <SDL.h>

class SpriteAtlas{
	SDL_Surface* atlas;
	unsigned     sprite_x;
	unsigned     sprite_y;
	unsigned     sprites_per_row;
public:
	void drawSprite(SDL_Surface* dest,
				    const int x,
				    const int y,
				    const unsigned which);

	SpriteAtlas(const char* filename,
	            const unsigned p_sprite_x,
				const unsigned p_sprite_y,
				const unsigned p_sprow);
};
