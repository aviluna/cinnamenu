// This file is part of Project Cerulean

#include <iostream>
#include <vector>

#include <regex.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>

#include <SDL.h>

#include "font.h"
#include "sprite.h"
#include "menu.h"

SDL_Surface* gfx_screen;

using namespace std;

uint8_t* mmapChunk(const char * filename, const unsigned size){
	int fd = open(filename, O_RDWR);
	if(!fd){
		perror("open");
		exit(1);
		return NULL;
	}
	uint8_t * map = (uint8_t *)
		mmap(	(caddr_t) 0,
				size,
				PROT_READ | PROT_WRITE,
				MAP_SHARED,
				fd,
				0);
	if(!map){
		perror("mmap");
		exit(1);
		return NULL;
	}
	close(fd);

	return map;
}

bool init(){
	if(SDL_Init(SDL_INIT_VIDEO) < 0){
		cerr	<< "SDL could not initialize! SDL_Error: " << SDL_GetError()
				<< endl;
		return false;
	}
	
	atexit(SDL_Quit);

	gfx_screen = SDL_SetVideoMode(320,
								   240,
								   16,
								   SDL_ANYFORMAT);
	if(!gfx_screen){
		cerr	<< "Window could not be created: " << SDL_GetError()
				<< endl;
		return false;
	}

	return true;
}

int main(){
	if(!init()){
		return false;
	}

	Font font("font.bmp", 8, 12, 16);
	SpriteAtlas deco("deco.bmp", 8, 12, 8);

#include "menu_config.cpp"

	MainMenu.open(gfx_screen, 0, 0);

	SDL_UpdateRect(gfx_screen, 0, 0, 320, 240);

	while(true){
		SDL_Event event;
		if(SDL_WaitEvent(&event) == 0){
			return 1; // event read failed
		}
		do{
			if(event.type == SDL_QUIT){
				return 0;
			}
			else if(event.type == SDL_KEYDOWN){
				switch(event.key.keysym.sym){
				case SDLK_UP:
					menuCursorUp();
					break;
				case SDLK_DOWN:
					menuCursorDown();
					break;
				case SDLK_RIGHT:
				case SDLK_RETURN:
					menuChoose();
					break;
				case SDLK_LEFT:
				case SDLK_ESCAPE:
					// don't close root menu this way
					if(menuMultipleOpen())
						menuClose();
					break;
				}
			}
		} while(SDL_PollEvent(&event));
		
		SDL_UpdateRect(gfx_screen, 0, 0, 320, 240);
	}
	return 1;
}

