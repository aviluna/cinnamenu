#include <iostream>

#include <SDL.h>

#include "font.h"

using namespace std;

Font::Font(const char* filename,
		   const unsigned p_char_x,
		   const unsigned p_char_y,
		   const unsigned p_chars_per_row){
	atlas = SDL_LoadBMP(filename);
	if(!atlas){
		cerr << "Failed to load " << filename << "!" << endl;
		exit(1);
	}

	char_x = p_char_x;
	char_y = p_char_y;
	chars_per_row = p_chars_per_row;
}

void Font::drawText(SDL_Surface* dest,
					const int x_initial,
					const int y_initial,
					const char* text){
	int x = x_initial;
	int y = y_initial;
	for(; *text; text++){
		char ch = *text;
		if(ch == '\n' || (x + char_x) > dest->w){
			y += char_y;
			x = x_initial;
		}
/*
		else if(ch == ' '){
			x += char_x;
		}
*/
		else if(ch < ' ' || ch > '~'){ // if not within ascii printable
			// do nothing; discard the character
		}
		else{
			ch -= ' '; // shift into the range we want

			SDL_Rect src = {
				.x = (ch % chars_per_row) * char_x,
				.y = (ch / chars_per_row) * char_y,
				.w = char_x,
				.h = char_y
			};
			SDL_Rect dst = {
				.x = x,
				.y = y
			};
			SDL_BlitSurface(atlas, &src, dest, &dst);
			x += char_x;
		}
	}
}


