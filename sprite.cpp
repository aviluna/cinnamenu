#include <iostream>

#include <SDL.h>

#include "sprite.h"

using namespace std;

SpriteAtlas::SpriteAtlas(const char* filename,
                         const unsigned p_sprite_x,
			             const unsigned p_sprite_y,
			             const unsigned p_sprow){
	atlas = SDL_LoadBMP(filename);
	if(!atlas){
		cerr << "Failed to load " << filename << "!" << endl;
		exit(1);
	}

	sprite_x = p_sprite_x;
	sprite_y = p_sprite_y;
	sprites_per_row = p_sprow;
}

void SpriteAtlas::drawSprite(SDL_Surface* dest,
		const int x,
		const int y,
		const unsigned which){
		SDL_Rect src = {
			.x = (which % sprites_per_row) * sprite_x,
			.y = (which / sprites_per_row) * sprite_y,
			.w = sprite_x,
			.h = sprite_y
		};
		SDL_Rect dst = {
			.x = x,
			.y = y
		};
		SDL_BlitSurface(atlas, &src, dest, &dst);
}

