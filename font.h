#pragma once

#include <SDL.h>

class Font{
	SDL_Surface* atlas;
	unsigned     char_x;
	unsigned     char_y;
	unsigned     chars_per_row;
public:
	void drawText(SDL_Surface* dest,
				  const int x_initial,
				  const int y_initial,
				  const char* text);

	Font(const char* filename,
		 const unsigned p_char_x,
		 const unsigned p_char_y,
		 const unsigned p_chars_per_row);
};
