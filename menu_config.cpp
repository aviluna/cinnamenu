// Feel free to allocate memory and let it leak. This stuff only gets run once,
// and program destruction cleans up everything anyway.

MenuEntry GamesMenuEntries[] = {
	{
		.name = "Pac-Man",
		.action = menuStubAction
	},
	{
		.name = "Pokemon",
		.action = menuStubAction
	},
	{
		.name = "(Cancel)",
		.action = menuCloseAction
	}
};
Menu GamesMenu("Games",
			   GamesMenuEntries,
			   sizeof(GamesMenuEntries) / sizeof(MenuEntry),
			   &font,
			   &deco);

MenuEntry* GameboyMenuEntries = NULL;
unsigned   GameboyMenuEntryCount = 0;

{
	// ugly, but it works
	// WARNING: crafted filenames may lead to arbitrary execution by escaping " in execcall
	
	vector<MenuEntry> gm_e;

	regex_t gbfile;
	if(regcomp(&gbfile, "(.*)\\.((gbc?)|(dmg))$", REG_ICASE | REG_EXTENDED)){
		cerr << "Failed to compile gbfile regex!" << endl;
		exit(1);
	}

	struct dirent* ent;
	DIR * dir = opendir("gameboy");
	if(dir){
		while((ent = readdir(dir)) != NULL){
			regmatch_t matches[2];
			if((regexec(&gbfile, ent->d_name, 2, matches, 0) == 0)){
				size_t namesize = (matches[1].rm_eo - matches[1].rm_so) + 1;
				size_t callsize =
					strlen(ent->d_name) +
					strlen("gambatte-sdl -f -i return backspace z x up down left right \"gameboy/\"") +
					1;
				char* romname = (char*) calloc(namesize,1);
				char* execcall = (char*) malloc(callsize);
				memcpy(romname,
					   ent->d_name + matches[1].rm_so,
					   namesize - 1);
				snprintf(execcall, callsize, "gambatte-sdl -f -i return backspace z x up down left right \"gameboy/%s\"", ent->d_name);
				gm_e.push_back(MenuEntry{
						.name = romname,
						.action = menuLaunchAction,
						.data = execcall
						});
			}
		}
		closedir(dir);
	}
	gm_e.push_back(MenuEntry{
			.name = "(Cancel)",
			.action = menuCloseAction
			});
	GameboyMenuEntries = (MenuEntry*) malloc(gm_e.size() * sizeof(MenuEntry));
	unsigned i = 0;
	for(vector<MenuEntry>::iterator it = gm_e.begin(); it != gm_e.end(); ++it, i++){
		memcpy(GameboyMenuEntries + i, &(*it), sizeof(MenuEntry));
	}
	GameboyMenuEntryCount = gm_e.size();
}

Menu GameboyMenu("Gameboy ROMs",
				 GameboyMenuEntries,
				 GameboyMenuEntryCount,
				 &font,
				 &deco);

MenuEntry EmulatorMenuEntries[] = {
	{
		.name = "Gameboy",
		.action = menuOpenAction,
		.data = &GameboyMenu
	},
	{
		.name = "(Cancel)",
		.action = menuCloseAction
	}
};

Menu EmulatorMenu("Emulators",
				  EmulatorMenuEntries,
				  sizeof(EmulatorMenuEntries) / sizeof(MenuEntry),
				  &font,
				  &deco);

MenuEntry MainMenuEntries[] = {
	{
		.name = "Games",
		.action = menuOpenAction,
		.data = &GamesMenu
	},
	{
		.name = "Emulators",
		.action = menuOpenAction,
		.data = &EmulatorMenu
	},
	{
		.name = "Programs",
		.action = menuStubAction
	},
	{
		.name = "Options",
		.action = menuStubAction
	},
	{
		.name = "Turn Off",
		.action = menuCloseAction
	}
};
Menu MainMenu("Cinnabar Menu",
			  MainMenuEntries,
			  sizeof(MainMenuEntries) / sizeof(MenuEntry),
			  &font,
			  &deco);

